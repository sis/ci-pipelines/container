.ONESHELL:
HADOLINT_VERSION ?= 2.12.0
TRIVY_VERSION ?= 0.37.1

DOCKER ?= buildah
REGISTRY ?= docker.io/example
VERSION ?= 1.0
TRIVY_IMAGE ?= docker.io/aquasec/trivy
HADOLINT_IMAGE ?= docker.io/hadolint/hadolint


install-tools:
	set -e
	wget https://github.com/hadolint/hadolint/releases/download/v$(HADOLINT_VERSION)/hadolint-Linux-x86_64
	wget https://github.com/aquasecurity/trivy/releases/download/v$(TRIVY_VERSION)/trivy_$(TRIVY_VERSION)_Linux-64bit.tar.gz
	tar -xvf trivy*tar.gz
	chmod a+x hadolint-Linux-x86_64 trivy

build-container:
	$(DOCKER) build -t example:$(VERSION) -f Dockerfile .

test-container: test-hadolint test-trivy

test-hadolint:
	./hadolint-Linux-x86_64 Dockerfile

test-trivy:
	$(DOCKER) push example:$(VERSION) oci:image
	./trivy image --input image --exit-code 1

push-container:
	$(DOCKER) tag slurm-api:$(VERSION) $(REGISTRY)/example:$(VERSION)
	$(DOCKER) push $(REGISTRY)/slurm-api:$(VERSION)
