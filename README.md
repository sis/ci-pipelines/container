# Container

This pipeline checks:
 - The Dockerfile works as expected
 - Push the image to a registry (line commented for this example)
 - Check the image against known vulnerabilities

## Using the last version of Hadolint and Trivy

In the Makefile, you can find the versions at the beginning.

Here you can check the last version of hadolint: https://github.com/hadolint/hadolint/releases/latest

Trivy is here: https://github.com/aquasecurity/trivy/releases/latest/
